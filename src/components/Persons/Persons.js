import React, { Component } from "react";
import Person from "./Person/Person";

class Persons extends Component {
  constructor(props) {
    super(props);
    console.log("Persons constructor");
  }

  componentWillMount() {
    console.log("Persons.js Component will mount");
  }

  componentDidMount() {
    console.log("Persons.js Component did mount");
  }

  componentWillReceiveProps(nextProps) {
    console.log("Persons.js 'component will receive Props'", nextProps);
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("Persons.js 'shouldComponentUpdate'", nextProps, nextState);
  }

  render() {
    console.log("Persons render method");
    return this.props.persons.map((person, index) => {
      return (
        <Person
          click={() => this.props.deletePersonHandler(index)}
          name={person.name}
          age={person.age}
          changed={event => this.props.nameChangedHandler(event, person.id)}
        />
      );
    });
  }
}

export default Persons;
