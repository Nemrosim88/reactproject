import React, { Component } from "react";
import cssClasses from "./Person.css";

/**
 * When using claa-based components -> this.properties
 * @param {} properties
 */
class Person extends Component {
  constructor(props) {
    super();
    console.log("Person.js -> constructor");
  }

  render() {
    return (
      <div className={cssClasses.Person}>
        <p onClick={this.props.click}>
          Hello, {this.props.name}! Do you have{" "}
          {Math.floor(Math.random() * this.props.dollars)} dollars?
        </p>
        <b>Click on text to delete element</b>
        <p>{this.props.children}</p>
        <input
          type="text"
          onChange={this.props.changed}
          value={this.props.name}
        />
        <b>Type new name in the input to change text name</b>
      </div>
    );
  }
}

export default Person;
