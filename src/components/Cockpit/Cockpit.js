import React from "react";
import logo from "./../../logo.svg";
import cssClasses from "./Cockpit.css";

/**
 * When using claa-based components -> this.properties
 * @param {} properties
 */
const cockpit = properties => {
  // It is converting array to a string "joined"
  // with a space -> "red bold"
  let cssClass = ["red", "bold"].join(" ");

  let newVersinOfCssClasses = [];

  let btnClass = "";
  if (properties.showPersons) {
    btnClass = cssClasses.Red;
  }

  

  if (properties.persons.length <= 2) {
    newVersinOfCssClasses.push(cssClasses.red);
  }
  if (properties.persons.length <= 1) {
    newVersinOfCssClasses.push(cssClasses.bold);
  }

  return (
    <div className={cssClasses.Cockpit}>
      <header className={cssClasses.AppHeader}>
        <img src={logo} className={cssClasses.AppLogo} alt="logo" />
        <h1 className={cssClasses.AppTitle}>Welcome to React</h1>
      </header>
      <p className={cssClasses.AppIntro}>
        To get started, edit <code>src/App.js</code> and save to reload.
      </p>
      <p className={newVersinOfCssClasses.join(" ")}>Using cssClasses.</p>
      {/* <button onClick={this.switchNameHandler.bind(this, 'Mathew')}> */}

      <div>
        <button
          className={btnClass}
          onClick={() => properties.switchNameHandler("Mathew!!..!")}
          key="123"
        >
          Change name of the first person
        </button>
      </div>

      <div>
        <button
          className={btnClass}
          onClick={properties.togglePersonHandler}
          key="234"
        >
          Second button. Show persons or not?
        </button>
      </div>
    </div>
  );
};

export default cockpit;
