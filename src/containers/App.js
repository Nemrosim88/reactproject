import React, { Component } from "react";

import cssClasses from "./App.css";
import Person from "../components/Persons/Person/Person";
import Persons from "../components/Persons/Persons";
import Cockpit from "../components/Cockpit/Cockpit";
import ErrorBoundary from "../ErrorBoundary/ErrorBoundary";

class App extends Component {
  
  /**
   * 1
   * @param {*} props 
   */
  constructor(props) {
    super(props);

    // "state" is for component that is extended from Component
    // This is the old version of init.
    this.state = {
      persons: [
        { id: "asdas", name: "Artem", age: 12 },
        { id: "dfdf", name: "John", dollars: 50000 },
        { id: "23sdv", name: "Inokenty" }
      ],
      showPersons: false
    };
  }

  /**
   * 2
   */
  componentWillMount() {}

  /**
   * 4
   */
  componentDidMount(){}

  

  /**
   * If we will write in "onClick" event this.switchNameHandler() -> this will automatically start the process.
   * Instead we should write without () -> this.switchNameHandler
   * URL to supported events -> https://reactjs.org/docs/events.html#supported-events
   */
  switchNameHandler = newName => {
    // DONT DO THIS this.state.persons[0].name = "Martin";
    this.setState({
      persons: [
        { name: newName, age: 12 },
        { name: "John", dollars: 50000 },
        { name: "Inokenty" }
      ]
    });
  };

  /**
   * This is used in Person below.
   */
  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({ persons: persons });
  };

  /**
   * This event swithes one value in state - doesShow
   */
  togglePersonHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  };

  /**
   *
   */
  deletePersonHandler = personIndex => {
    // хорошая практика копировать лист перед манипуляциями с ним
    let persons = this.state.persons.slice();
    // новая версия
    persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({ persons: persons });
  };

  /**
   * 3
   */
  render() {




    let persons = null;
    let btnClass = "";

    /**
     * All this IF code can be replaced with (тернарное выражение в {} блоке)
     */
    if (this.state.showPersons) {
      persons = (
        <Persons
          persons={this.state.persons}
          deletePersonHandler={this.deletePersonHandler}
          nameChangedHandler={this.nameChangedHandler}
        />
      );
    }

    return (
      <div className={cssClasses.App}>
        <Cockpit
          showPersons={this.state.showPersons}
          persons={this.state.persons}
          switchNameHandler={this.switchNameHandler}
          togglePersonHandler={this.togglePersonHandler}
        />

        {persons}
      </div>
    );
  }
}

export default App;
